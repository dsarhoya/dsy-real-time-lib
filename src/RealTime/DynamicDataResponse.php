<?php

namespace DSY\RealTime;

class DynamicDataResponse
{
    private $id;
    private $slug;

    public function __construct($id, $slug)
    {
        $this->id = $id;
        $this->slug = $slug;
    }

    /**
     * Get the value of id.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of slug.
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
